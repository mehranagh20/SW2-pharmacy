from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^prescriptions/$', views.prescription, name='prescription'),
    url(r'^prescriptions/(\d+)/$', views.prescription_view, name='prescription_view'),
    url(r'^prescriptions/$', views.prescription_add, name='prescription_add'),
    url(r'^(?P<slug>[a-z0-9-_]+?)/$', views.page_details, name='details'),
]
