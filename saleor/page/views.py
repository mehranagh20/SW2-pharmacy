import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse_lazy

from saleor.cart.utils import set_cart_cookie, get_or_create_cart_from_request
from saleor.product.utils import products_for_cart, handle_cart_form
from .utils import pages_visible_to_user
from ..account.forms import PrescriptionFrom
from ..product.forms import ProductForm
from ..product.models import Product
from ..product.views import *
from ..account.utils import *
from django.urls import resolve
from django.shortcuts import redirect, render

from ..account.models import User

from ..account.models import PrescriptionHistory

from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth import views as django_views
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.utils.translation import pgettext, ugettext_lazy as _

from ..cart.utils import find_and_assign_anonymous_cart
from ..core.utils import get_paginator_items



PRISCRIPTIONS = {
    123: (Product.objects.all()[0:2], 1),
    456: (Product.objects.all(), 1),
}


def page_details(request, slug):
    page = get_object_or_404(
        pages_visible_to_user(user=request.user).filter(slug=slug))
    today = datetime.date.today()
    is_visible = (
        page.available_on is None or page.available_on <= today)
    return TemplateResponse(
        request, 'page/details.html', {
            'page': page, 'is_visible': is_visible})


def prescription_view(request, prescription_id):
    medicine_list = PRISCRIPTIONS[int(prescription_id)][0]
    context = {
        'medicine_list': medicine_list,
    }

    return TemplateResponse(request, 'page/prescription_submit.html', context)

@login_required
def prescription_add(request):
    form = PrescriptionFrom(request.POST or None)
    if form.is_valid():
        number = int(form.cleaned_data.get('number'))
        if number not in PRISCRIPTIONS:
            form.add_error('','Invalid prescription number')
            ctx = {'form': form}
            return TemplateResponse(request, 'page/prescription_form.html', ctx)

        # submit button clicked
        if request.POST.get("view"):
            return HttpResponseRedirect(reverse('page:prescription_view', args=[number]))

        if not PRISCRIPTIONS[int(form.cleaned_data.get('number'))][1]:
            form.add_error('', 'Prescription is already bought.')
            ctx = {'form': form}
            return TemplateResponse(request, 'page/prescription_form.html', ctx)

        # view button clicked
        elif request.POST.get("submit"):
            cart = get_or_create_cart_from_request(request)
            for p in PRISCRIPTIONS[int(form.cleaned_data.get('number'))][0]:
                form = ProductForm(
                    cart=cart, product=p, data={'quantity': '1', 'variant': str(p.variants.all()[0].id)} or None,
                    discounts=request.discounts, taxes=request.taxes)
                if form.is_valid():
                    form.save()


            PRISCRIPTIONS[number] = (PRISCRIPTIONS[number][0], 0)

            us = User.objects.get(id=request.user.id)
            q = PrescriptionHistory.objects.create(prescription_num=number, user_id=us)
            q.save()

            response = redirect('cart:index')
            if not request.user.is_authenticated:
                set_cart_cookie(cart, response)
            return response

    ctx = {'form': form}
    return TemplateResponse(request, 'page/prescription_form.html', ctx)
